<?php

namespace App\Models;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class,
            'permission_role',
            'role_id',
            'permission_id'
        );
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'role_user',
            'role_id',
            'user_id'
        );
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'Like', '%' . $name . '%');
    }

    public function scopeWithoutSuperAdmin($query)
    {
        return $query->where('name', '!=', 'super-admin');
    }

    public function addPermission($permissionId)
    {
        return $this->permissions()->attach($permissionId);
    }

    public function syncPermission($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }
}
