<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public const SUPER_ADMIN_NAME_ROLE = 'super-admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id',
        );
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'Like', '%' . $name . '%');
    }

    public function scopeWithEmail($query, $email)
    {
        return $query->orWhere('email', 'Like', '%' . $email . '%');
    }

    public function scopeWithRoleName($query, $role)
    {
        return $role ? $query->WhereHas('roles', fn ($q) => $q->where('roles.name', $role)) : null;
    }

    public function addRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId): array
    {
        return $this->roles()->sync($roleId);
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function hasPermission($permission): bool
    {
        foreach ($this->roles as $role) {
            if ($role->permissions->contains('name', $permission)) {
                return true;
            }
        }
        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole(self::SUPER_ADMIN_NAME_ROLE);
    }
}
