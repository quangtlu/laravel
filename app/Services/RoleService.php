<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected  $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['search'] = $request->search;
        return $this->roleRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        $role = $this->roleRepository->create($dataCreate);

        $role->addPermission($request->permission_id);

        return $role;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $role = $this->roleRepository->update($dataUpdate, $id);

        $role->syncPermission($request->permission_id);

        return $role;
    }

    public function findById($id)
    {
        return $this->roleRepository->findWithoutRedirect($id);
    }

    public function delete($id) : int
    {
        return $this->roleRepository->delete($id);
    }

    public function getPermissionId($id)
    {

        return $this->roleRepository->getPermissionId($id);
    }

    public function count()
    {
        return $this->roleRepository->countWithoutSuperAdmin();
    }

    public function withoutSuperAdmin()
    {
        return $this->roleRepository->getWithoutSuperAdmin();
    }
}
