<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    protected  $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->permissionRepository->all();
    }

    public function getWithGroupName($request)
    {
        return $this->permissionRepository->getWithGroupName($request);
    }

    public function findById($id)
    {
        return $this->permissionRepository->findWithoutRedirect($id);
    }
}
