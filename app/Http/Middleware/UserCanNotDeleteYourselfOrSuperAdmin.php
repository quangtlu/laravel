<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserCanNotDeleteYourselfOrSuperAdmin
{
    const SUPER_ADMIN_ID = 1;
    const GET_USER_ID = 2;
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::id() == $request->segment(self::GET_USER_ID) ||
            $request->segment(self::GET_USER_ID) == self::SUPER_ADMIN_ID) {
            abort(403);
        }
        return $next($request);
    }
}
