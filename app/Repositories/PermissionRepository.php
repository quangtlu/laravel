<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
    public function model() : string
    {
        return Permission::class;
    }

    public function getWithGroupName($groupColumn)
    {
        return $this->model->all()->groupBy($groupColumn);
    }
}
