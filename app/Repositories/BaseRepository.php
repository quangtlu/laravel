<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;

abstract class BaseRepository
{
    public $model;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @throws BindingResolutionException
     */
    public function makeModel()
    {
        $this->model = app()->make($this->model());
    }

    abstract public function model();

    /**
     * Get all data
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Find data by id
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function findWithoutRedirect($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Save a new entity in repository
     * @param array $input
     * @return mixed
     */
    public function create(array $input)
    {
        return $this->model->create($input);
    }

    /**
     * Update a entity in repository by id
     * @param array $input
     * @param $id
     * @return BaseRepository
     */
    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;
    }

    /**
     * Delete a entity in repository by id
     * @param $id
     * @return int
     */
    public function delete($id): int
    {
        return $this->model->destroy($id);
    }

    public function multipleDelete(array $ids)
    {
        return $this->model->destroy(array_values($ids));
    }

    /**
     * Count all records
     * @return mixed
     */
    public function count()
    {
        return $this->model->all()->count();
    }

    /**
     * Latest
     * @param $id
     * @return mixed
     */
    public function latest($id)
    {
        return $this->model->latest($id);
    }
}
