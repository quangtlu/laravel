<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function model() : string
    {
        return Role::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->withoutSuperAdmin()->latest('id')->paginate(5);
    }

    public function getPermissionId($id)
    {
        return $this->model->find($id)->permissions()->where('role_id', $id)->pluck('permission_id');
    }

    public function getWithoutSuperAdmin()
    {
        return $this->model->withoutSuperAdmin()->get();
    }

    public function countWithoutSuperAdmin()
    {
        return $this->model->withoutSuperAdmin()->count();
    }
}
